Au cours de ma formation 'Developpeur Web/ developpeur Mobile' , nous devions, en guise de sixième projet, créer une plateforme de Blog.
Les fonctionnalités obligatoires étaient les suvantes : 
- Inscription d'un user.
- Connection d'un user.
- Le user doit pouvoir poster des articles et les supprimer depuis son compte.

Les liens de ma maquettes fonctionnelles et diagrammes se trouvent ci dessous :

Maquette fonctionnelle :

https://whimsical.com/blog-VKz2HPJxSUC1ZP8wqz6Ufx 


Use Case : 

https://whimsical.com/ucblog-4LSTJSv2XMxZyRTVTiqV7z


Diagramme : 

https://lucid.app/lucidchart/invitations/accept/inv_ec5bc76c-e5ad-4cd5-b4e8-185ddac94b68?viewport_loc=-41%2C119%2C1480%2C648%2C0_0 

