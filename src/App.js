
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Switch } from 'react-router';
import './App.css';
import Navbar from './components/Navbar';
import Account from './pages/Account';
import Home from './pages/Home';
import Signin from './pages/Signin';
import Signup from './pages/Signup';
import { loginWithToken } from './stores/auth-slice';

function App() {
  let dispatch = useDispatch()
  useEffect(()=>{
    dispatch(loginWithToken())
  })
  return (
    <div>
      <Navbar />
      <Switch >

        <Route path="/" exact >
          <Home />
        </Route>

        <Route  path="/signup" >
          <Signup />
        </Route>

        <Route path="/signin">
          <Signin  />
        </Route>

        <Route path="/account" >
          <Account />
        </Route>



      </Switch>
    </div>

  );
}

export default App;
