import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import LoginForm from '../components/LoginForm';

import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../stores/auth-slice';

import 'antd/dist/antd.css'


export default function Signin() {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user);

  //Faire un if sur le button ( si user : afficher le button, si non : afficher le button "logout" avec le dispatch logout dessus)

  return (
    <>

      {
        !user ? <Button style={{ backgroundColor: '#00695c', height: '6vh', margin: '7px', fontWeight: 'bolder', borderRadius: "5px", color: "orangered" }} onClick={showModal}>
          Signin
      </Button> :
          <Button key="/logout" onClick={() => dispatch(logout())} style={{ backgroundColor: '#00695c', height: '6vh', margin: '7px', fontWeight: 'bolder', borderRadius: "5px", color: "orangered" }} >
            Logout
      </Button>  

      }

      <Modal title="Welcome Back" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <LoginForm />
      </Modal>
    </>
  );
};