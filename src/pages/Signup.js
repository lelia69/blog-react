import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import RegisterForm from '../components/RegisterForm';

export default function Signup () {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="primary" style={{backgroundColor: "#e0f2f1", borderRadius: '5px', color : '#00695c'}} onClick={showModal}>
        Signup
      </Button>
      <Modal title="Welcome" style={{borderColor: "orangered, 2px solid"}} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <RegisterForm/>
      </Modal>
    </>
  );
};