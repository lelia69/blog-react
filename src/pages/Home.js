
import TopicList from '../components/TopicList'
import './Home.css'
import PlayForWorkOutlinedIcon from '@material-ui/icons/PlayForWorkOutlined';
export default function Home() {

    return (
        <div className="contain">
            <article>
                <h1 className="title">
                    Write, Read, and <br /> <span className="spanou">Connect ...</span>
                </h1>
            </article>
            <section>

            </section>
            <article className="post">
                <p className="posts"> POSTS <PlayForWorkOutlinedIcon /> </p>
            </article>

            <TopicList />
            <footer>
                <p>©Copyright</p>
            </footer>
        </div>

    )
}