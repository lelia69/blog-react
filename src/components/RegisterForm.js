
import { Form, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css'
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../stores/auth-slice';
import * as filestack from "filestack-js";
import { useState } from 'react';
import { DownloadOutlined } from '@ant-design/icons';


export default function RegisterForm() {

    const dispatch = useDispatch();
    const [image, setImage] = useState(null)
    const feedback = useSelector(state => state.auth.registerFeedback);
   

    const onFinish = (values) => {
        dispatch(register({...values, avatar: image}));
    }

    const client = filestack.init('AREYv4BEcS7Kac5TzhVrGz');
    const options = {
        onFileSelected: file => {
            if (file.size > 1000 * 1000) {
                throw new Error('File too big, select something smaller than 1MB');
            }
        },
        transformations: {
            crop: {
                force: true,
                aspectRatio: 1 / 1
            }
        },
        accept: ["image/*"],
        uploadInBackground: false,
        onUploadDone: handleImage,



    };

    function handleImage(result) {
      setImage(result.filesUploaded[0].url)
     
      
  }



  
    
  return (
    <Form
      name="basic"
      labelCol={{
        span: 6,
        
        
      }}
      wrapperCol={{
        span: 13,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      
    >
      {feedback && <p>{feedback}</p>}
      <Form.Item
        label="Pseudo"
        name="pseudo"
        rules={[
          {
            required: true,
            message: 'Please input your Pseudo!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Avatar"
        name="avatar"
        rules={[
          {
            
            message: 'Please input your Avatar!',
          },
        ]}
      >
         <Button onClick={() => client.picker(options).open()} type="primary" ghost icon={<DownloadOutlined />} style={{ marginRight: 5 }} >
          Download
        </Button>
      </Form.Item>

      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            type : 'email',
            message: 'Please input your Email!',
          },
        ]}
      >
        <Input type="email" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="pwd"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>


      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
