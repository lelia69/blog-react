
import 'antd/dist/antd.css'
import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { createTopic, setuserTopiclist } from '../stores/topic-slice';
import * as filestack from "filestack-js";
import { useState } from 'react';
import { DownloadOutlined } from '@ant-design/icons';



const layouts = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 9,
  },

};


const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};





const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};




export default function PostForm() {

  
  const dispatch = useDispatch()
  const [image, setImage] = useState(null)
  
  let newTopics= useSelector(state=> state.topicS.userTopiclist)
  let user = useSelector(state => state.auth.user)

  const onFinish = (values) => {
    dispatch(createTopic({ ...values, picture: image }))
    dispatch(setuserTopiclist([...newTopics, {...values, picture: image, user: user, userid: user.userid }]))
  };

  const client = filestack.init('AREYv4BEcS7Kac5TzhVrGz');
  const options = {
    onFileSelected: file => {
      if (file.size > 1000 * 1000) {
        throw new Error('File too big, select something smaller than 1MB');
      }
    },
    transformations: {
      crop: {
        force: true,
        aspectRatio: 16 / 9
      }
    },
    accept: ["image/*"],
    uploadInBackground: false,
    onUploadDone: handleImage,



  };

  function handleImage(result) {
    setImage(result.filesUploaded[0].url)


  }



  return (

    <Form {...layouts} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>

      <h3 style={{ textAlign: "center" }}>New Posts</h3>
      <br />
      <Form.Item
        name={['title']}
        label="title"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>


      <Form.Item name={['category']} label="Category">
        <Input />
      </Form.Item>


      <Form.Item name={['description']} label="Description">
        <Input.TextArea />
      </Form.Item>

      <Form.Item
        name={['date']}
        label="Date"
        rules={[
          {
            type: 'date',

          },
        ]}
      >
        <Input type="date" />
      </Form.Item>
     
      <Form.Item
        label="Picture"
        name="picture"
        rules={[
          {

            message: 'Please input your Avatar!',
          },
        ]}
      >
        <Button onClick={() => client.picker(options).open()} type="primary"  icon={<DownloadOutlined />} style={{ marginRight: 5 }} >
          Download
        </Button>
      </Form.Item>




      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}></Form.Item>

      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Button type="primary" htmlType="submit" >
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
