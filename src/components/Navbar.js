
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import teal from '@material-ui/core/colors/teal';
import SvgIcon from '@material-ui/core/SvgIcon';
import { Link } from 'react-router-dom';
import Signup from '../pages/Signup';
import Signin from '../pages/Signin';
import { useSelector } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import { withStyles, createStyles } from '@material-ui/core/styles';




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),

    },
    title: {
        flexGrow: 1,
        textAlign: 'center',
        color: teal[50],
    },

    app: {
        backgroundColor: teal[400]
    },

    btn1: {
        backgroundColor: teal[50],
        width: 85,
    },

    btn2: {
        backgroundColor: teal[800],
        color: teal[50],
        marginLeft: theme.spacing(3),
        fontWeight: 'lighter',
        height: 30,



    },

    linkou: {
        textDecoration: 'none',
        color: teal[400],

    },

    linkou2: {
        textDecoration: 'none',
        color: teal[50],
    },

    avatar: {
        width: theme.spacing(6),
        height: theme.spacing(6),
        marginRight: 7
    },


}));


const StyledBadge = withStyles((theme) =>
    createStyles({
        badge: {
            backgroundColor: '#44b700',
            color: '#44b700',
            boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
            '&::after': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                borderRadius: '50%',
                animation: '$ripple 1.2s infinite ease-in-out',
                border: '1px solid currentColor',
                content: '""',
            },
        },

        '@keyframes ripple': {
            '0%': {
                transform: 'scale(.8)',
                opacity: 1,
            },
            '100%': {
                transform: 'scale(2.4)',
                opacity: 0,
            },
        },
    }),
)(Badge);

export default function Navbar() {
    const classes = useStyles();
    const user = useSelector(state => state.auth.user);

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.app}>
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <Link className={classes.linkou2} to="/">  <SvgIcon style={{ fontSize: 30 }} >
                            <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
                        </SvgIcon></Link>
                    </IconButton>
                    <Typography variant="h5" className={classes.title}>
                        W.R.C
                    </Typography>

                    {user && <Link to="/account">
                        <div className={classes.root}>
                            <StyledBadge
                                overlap="circular"
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                variant="dot"
                            >
                                <Avatar className={classes.avatar} alt="Avatar" src={user.avatar} />
                            </StyledBadge>
                            <Badge
                                overlap="circular"
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}

                            >

                            </Badge>
                        </div>
                    </Link>}


                    <Signin />
                    <Signup />

                </Toolbar>
            </AppBar>
        </div>
    );
}

