import { IconButton } from "@material-ui/core";
import { useState } from "react";
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';

export default function Counter() {
    
    const [counter, setCount] = useState(0)
  
    return (
      <div>
        
        <IconButton aria-label="share">
          <ThumbUpAltIcon onClick={() => setCount(counter + 1)} /> 
        </IconButton>
        <span>{counter}</span>
        <IconButton aria-label="share">
          <ThumbDownIcon onClick={() => setCount(counter - 1)} /> 
        </IconButton>
      </div>
    );
  }