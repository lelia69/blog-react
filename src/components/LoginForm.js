
import { Form, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css'
import { useDispatch, useSelector } from 'react-redux';
import { loginWithCredentials } from '../stores/auth-slice';

export default function LoginForm() {


  const dispatch = useDispatch();
  const feedback = useSelector(state => state.auth.loginFeedback);

  const onFinish = (values) => {
    dispatch(loginWithCredentials(values));

  }
  return (

    <Form 
      name="basic"
      labelCol={{
        span: 6,


      }}
      wrapperCol={{
        span: 13,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}

      
    >


      {feedback}
      <Form.Item 
        label="Pseudo"
        name="pseudo"
        rules={[
          {
            
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            type : "email",
            message: 'Please input your username!',
          },
        ]}
      >
        <Input type="email"/>
      </Form.Item>

      <Form.Item
        label="Password"
        name="pwd"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};