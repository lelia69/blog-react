import { Col, Row } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchTopics } from "../stores/topic-slice";
import Topics2 from "./Topics2";



export default function TopicList (){

    const topiclist = useSelector(state=> state.topicS.topiclist);
    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTopics())
    }, [dispatch])


    return(
        <div>
            <Row>
                {topiclist.map(item => (
                <Col span={6}>
                    <Topics2 key={item.topicid} topic={item} />
                </Col>
                
            ))}
            </Row>
            
        </div>
    )
}