import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { grey, red } from '@material-ui/core/colors';

import moment from 'moment';
import Counter from './Counter';
import './essai1.css'
import { useDispatch, useSelector } from 'react-redux';
import { deleteTopics, setuserTopiclist } from '../stores/topic-slice';
import { Button } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';




const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
        marginLeft: 30,
        backgroundColor: "rgb(121, 83, 72, 0.65)",
        borderLeft: "1px solid orangered",
        borderRight: "1px solid orangered",
        color: grey[100]
        //boxShadow: " -10px 0px 13px -7px #000000, 10px 0px 13px -7px #000000, 5px 2px 8px 3px rgba(51,51,51,0);"




    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9 
        borderBottom: "1px solid orangered"



    },

    avatar: {
        backgroundColor: red[500],
        width: theme.spacing(7),
        height: theme.spacing(7),
    },

    header: {
        borderBottom: "1px solid orangered",

    }
}));

export default function UserTopic({ topic }) {

    const classes = useStyles();
    let userTopic = useSelector(state => state.topicS.userTopiclist)
    const dispatch = useDispatch();

    const onDelete = (id) => {
        dispatch(deleteTopics(id));
        dispatch(setuserTopiclist(userTopic.filter(item => item.topicid !== id)))
    }

    console.log(topic);
    return (


        <Card className={classes.root}>
            <CardHeader className={classes.header}


                action={
                    <IconButton aria-label="settings">
                        <p className="pseudo"> {topic.user.pseudo} </p>
                    </IconButton>

                }
                title={topic.title}
                subheader={moment(topic.date).fromNow()}

                avatar={
                    <Avatar className={classes.avatar} src={topic.user.avatar} />
                }
            />



            <CardMedia
                className={classes.media}
                image={topic.picture}
                title="topic name"
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {topic.description}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>

                <Counter />
                <Button onClick={() => onDelete(topic.topicid)} style={{ backgroundColor: "#f44336", marginLeft: 60 }}
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    startIcon={<DeleteIcon />}
                >
                    Delete
                </Button>

            </CardActions>
        </Card>



    );
}
