import { Col, Row } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { displayUserTopic } from "../stores/topic-slice";
import UserTopic from "./UserTopic";



export default function UserTopicList (){

    
    const topiclist = useSelector(state=> state.topicS.userTopiclist);
    const user = useSelector(state=> state.auth.user)

    
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(displayUserTopic(user.userid))
        console.log(topiclist);
    }, [dispatch, user])


    return(
        <div>
            { <Row>
                {(topiclist && user ) && topiclist.map(item => (
                <Col span={6}>
                    <UserTopic key={item.topicid} topic={item} />
                </Col>
                
            ))}
            </Row> }
            
        </div>
    )
}