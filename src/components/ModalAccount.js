import React, { useState } from 'react';
import { Modal } from 'antd';
import PostForm from './PostForm';
import { createStyles, Fab, makeStyles, Tooltip } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { teal } from '@material-ui/core/colors';
import Zoom from '@material-ui/core/Zoom';



const useStyles = makeStyles((theme) =>
  createStyles({
    fab: {
      margin: theme.spacing(3),
      backgroundColor:  teal[400],
      

    },

  }),
);

export default function ModalAccount () {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const classes = useStyles();

  return (
    <>
      <Tooltip TransitionComponent={Zoom} title="add" placement="bottom-end" aria-label="add" arrow >
        <Fab size="medium" color="primary" className={classes.fab}  onClick={showModal}>
          <AddIcon />
        </Fab>
      </Tooltip>
      
      <Modal visible={isModalVisible} onOk={handleOk} onSubmit={handleCancel}>
        <PostForm/>
      </Modal>
    </>
  );
};