

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import moment from 'moment';
import Counter from './Counter';
import './essai1.css'



const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    marginLeft: 30,
    backgroundColor: "rgb(199, 108, 82, 0.70)",


  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },

  avatar: {
    backgroundColor: red[500],
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

export default function Topics2({ topic }) {
  const classes = useStyles();
  console.log(topic);
  return (
  

        <Card className={classes.root}>
          <CardHeader
            avatar={
              <Avatar className={classes.avatar} src={topic.user.avatar} />
            }
            action={
              <IconButton aria-label="settings">
                <p className="pseudo"> {topic.user.pseudo} </p>
              </IconButton>

            }
            title={topic.title}
            subheader={moment(topic.date).fromNow()}
          />
          <CardMedia
            className={classes.media}
            image={topic.picture}
            title="Paella dish"
          />
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
              {topic.description}
            </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <IconButton aria-label="add to favorites">
              <FavoriteIcon />
            </IconButton>
            <Counter />
          </CardActions>
        </Card>

     

  );
}
