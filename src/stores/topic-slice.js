import { createSlice } from "@reduxjs/toolkit";
import Topicservice from "../service/Topicservice";



const topicSlice = createSlice({

    name: 'topicS',
    initialState : {topiclist : [], tops : {}, userTopiclist : []},
    reducers : {

        setTopic(state, {payload}){
            state.topiclist = payload
        },

        setuserTopiclist(state, {payload}){
            state.userTopiclist = payload
        },

        newTopic(state, {payload}){
            state.tops = payload

        } 
    }
});


export const {setTopic, newTopic, setuserTopiclist} = topicSlice.actions;
export default topicSlice.reducer;

export const fetchTopics = () => async (dispatch) => {
    try {
        const response = await Topicservice.fetchTopics()
        dispatch(setTopic(response));
    } catch (error) {
        console.log(error);
    }
}

export const createTopic = (topic) => async (dispatch) => {
    try{
        const response = await Topicservice.addTopic(topic)
        dispatch(newTopic(response))
        

    } catch(error){
        console.log(error);
    }
}


export const displayUserTopic = (id) => async (dispatch) => {
    try {
        const response = await Topicservice.findTopicSbyUser(id)
        
        if (response){
             dispatch(setuserTopiclist(response))
        }
       console.log(response);
    } catch(error){
        console.log(error);
    }
}

export const deleteTopics = (id) => async (dispatch) =>{
    try {
        const response = await Topicservice.deleteTopic(id)
        if (response){
            dispatch(setuserTopiclist(response))
        }
    } catch(error){
        console.log(error);
    }
}
