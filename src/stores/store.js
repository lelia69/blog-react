import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./auth-slice";
import topicSlice from "./topic-slice";
import userSlice from "./user-slice";



export const store = configureStore({
    reducer: {
        auth: authSlice,
        topicS : topicSlice,
        userS : userSlice,
        
    }
});
