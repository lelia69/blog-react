import { createSlice } from "@reduxjs/toolkit";
import AuthService from "../service/Authservice";





const authSlice = createSlice({

    name : 'auth',
    initialState : {user : false, loginFeedback : " ", registerFeedback : " "},
    reducers : {

        login(state, {payload}){
            state.user = payload;
        },

        logout(state){
            state.user = null;
            localStorage.removeItem('token')
        },

        updateLoginFeedback(state, {payload}){
            state.loginFeedback = payload;
        },

        updateRegisterFeedback(state,{payload}){
            state.registerFeedback = payload;
        }
    }

})

export const {login, logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;
export default authSlice.reducer;



export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem('token');

    if(token) {
        try {

            const user = await AuthService.fetchAccount();

            dispatch(login(user));
        } catch(error) {
            console.log(error);
            dispatch(logout());
        }
    } else {

        dispatch(logout());
    }
    
}



export const register = (param) => async (dispatch) => {
    try {
        const {user, token} = await AuthService.register(param);
        localStorage.setItem('token', token);

        dispatch(updateRegisterFeedback('Registration successful'));

        dispatch(login(user));

        

    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
        
    }
}


export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Login successful'));
        dispatch(login(user));

    } catch (error) {
        dispatch(updateLoginFeedback('Email and/or password incorrect'));
    }
}



