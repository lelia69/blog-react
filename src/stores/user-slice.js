import { createSlice } from "@reduxjs/toolkit";
import AuthService from "../service/Authservice";



const userSlice = createSlice({
    name : 'userS',
    initialState : { list : []},
    reducers : {

        setList(state, {payload}){
            state.list = payload;
        }
    }
})

 export  const {setList} = userSlice.actions;

 export default userSlice.reducer;



  export const displayUsers = () => async (dispatch) => {
    try {
        const response = await AuthService.fetchUsers();
        dispatch(setList(response));
    } catch (error) {
        console.log(error);
    }
}
