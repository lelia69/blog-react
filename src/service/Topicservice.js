
import axios from "axios";




export default class Topicservice {

    static async fetchTopics() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/topic');
        
        return response.data;
    }

    static async addTopic(topic) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/topic', topic);

        return response.data;
    }


    
    static async findTopicSbyUser(id){

        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/topic/'+ id);
        
        return response.data;
        

    }


    static async deleteTopic(id){
        const response = await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/topic/'+ id);

        return response.data
    }
   
}